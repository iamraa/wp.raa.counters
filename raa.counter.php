<?php
/*
Plugin Name: RAA.Counters
Description: Add counters: Google Analytics, Yandex.Metrika
Version: 0.1
License: GPL2

*/

/**
 * Class RaaCounter
 *
 * Add counters:
 * - Yandex.Metrika
 * - Google Analytics
 */
class RaaCounter {
	public static $options = [
		'ga'              => '',
		'accurate.bounce' => '0',
		'yam'             => '',
		'yam.webvisor'    => '0'
	];

	public static function header() {
		if ( defined( 'RAA_DISABLE_JS' ) ) {
			return;
		}
		self::getParams();

		// Show always
		if ( ! empty( trim( self::$options['ga'] ) ) ) {
			?>
            <script type="text/javascript">
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                            (i[r].q = i[r].q || []).push(arguments)
                        }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m)
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                ga('create', '<?= self::$options['ga'] ?>', '<?= parse_url( get_site_url(), PHP_URL_HOST ) ?>');
                ga('require', 'displayfeatures');
                ga('send', 'pageview');

				<?php if (! empty( self::$options['accurate.bounce'] )): ?>
                setTimeout("ga('send', 'event', 'NoBounce', 'Over 20 seconds');", 20000);
				<?php endif; ?>

            </script>
			<?php
		}

	}

	protected static function getParams() {
		$options = get_option( 'raa-counter' );
		if ( ! empty( $options ) ) {
			self::$options = $options;
		}

	}

	public static function footer() {
		if ( defined( 'RAA_DISABLE_JS' ) ) {
			return;
		}
		self::getParams();
		if ( ! empty( trim( self::$options['yam'] ) ) ) {
			?>
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript">
                (function (d, w, c) {
                    (w[c] = w[c] || []).push(function () {
                        try {
                            w.yaCounter<?= self::$options['yam']; ?> = new Ya.Metrika({
                                id:<?= self::$options['yam']; ?>,
                                clickmap: true,
                                trackLinks: true,
                                accurateTrackBounce: <?= ! empty( self::$options['accurate.bounce'] ) ? 'true'
								: 'false'; ?>,
                                webvisor: <?= ! empty( self::$options['yam.webvisor'] ) ? 'true' : 'false'; ?>
                            });
                        } catch (e) {
                        }
                    });

                    var n = d.getElementsByTagName("script")[0],
                        s = d.createElement("script"),
                        f = function () {
                            n.parentNode.insertBefore(s, n);
                        };
                    s.type = "text/javascript";
                    s.async = true;
                    s.src = "https://mc.yandex.ru/metrika/watch.js";

                    if (w.opera == "[object Opera]") {
                        d.addEventListener("DOMContentLoaded", f, false);
                    } else {
                        f();
                    }
                })(document, window, "yandex_metrika_callbacks");
                function yam_event(category, action, label, value) {
                    yaCounter<?= self::$options['yam']; ?>.reachGoal(action || '', {
                        name: action || '',
                        type: category || '',
                        value: label || '',
                        price: value || 0
                    });
                }
            </script>
            <noscript>
                <div><img src="https://mc.yandex.ru/watch/<?= self::$options['yam']; ?>"
                          style="position:absolute; left:-9999px;" alt=""/></div>
            </noscript>
            <!-- /Yandex.Metrika counter -->
			<?php

		}

		if ( ! empty( trim( self::$options['yam'] ) ) || ! empty( trim( self::$options['ga'] ) ) ) {
		    /*
		     * Create goals in Ya.Metrike before use.
		     * */
			?>
            <!-- Events -->
            <script type="text/javascript">
                if (typeof jQuery != 'undefined') {
                    (function ($) {
                        $(document).ready(function () {
                            $('a.raa-counter-click').on('click', function () {
                                var href = $(this).prop('href');
                                var data = $(this).data();

                                if (typeof ga == 'function') {
                                    //ga('send', 'event', 'category', 'action', 'label', value);  // value is a number.
                                    ga('send', 'event', data['category'] || '', data['action'] || '', data['label'] || href, data['value'] || '');
                                }
                                if (typeof yam_event == 'function') {
                                    yam_event(data['category'] || '', data['action'] || '', data['label'] || href, data['value'] || '');
                                }
                            });
                        });
                    })(jQuery);
                }
            </script>
			<?php
		}

	}

	public static function informer() {
		self::getParams();
		if ( trim( self::$yamUID ) != '' ) {
			?>
            <!-- Yandex.Metrika informer -->
            <a href="https://metrika.yandex.ru/stat/?id=<?= self::$options['yam']; ?>&amp;from=informer"
               target="_blank"
               rel="nofollow"><img
                        src="//bs.yandex.ru/informer/<?= self::$options['yam']; ?>/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
                        style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика"
                        title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                        onclick="try{Ya.Metrika.informer({i:this,id:<?= self::$options['yam']; ?>,lang:'ru'});return false}catch(e){}"/></a>
            <!-- /Yandex.Metrika informer -->
			<?php
		}

	}

	public static function activate() {
		add_option( 'raa-counter', self::$options );

	}

	public static function deactive() {
		delete_option( 'raa-counter' );

	}

	public static function adminInit() {
		register_setting( 'raa-counter', 'raa-counter' );

	}

	public static function adminMenu() {
		add_options_page(
			'RAA.Counters', 'RAA.Counters',
			'manage_options', 'RaaCounter',
			[ 'RaaCounter', 'optionsPage' ]
		);

	}

	public static function optionsPage() {
		self::getParams();
		include dirname( __FILE__ ) . '/options.php';

	}

}

register_activation_hook( __FILE__, [ 'RaaCounter', 'activate' ] );
register_deactivation_hook( __FILE__, [ 'RaaCounter', 'deactive' ] );

if ( is_admin() ) {
	add_action( 'admin_init', [ 'RaaCounter', 'adminInit' ] );
	add_action( 'admin_menu', [ 'RaaCounter', 'adminMenu' ] );
} else {
	add_action( 'wp_head', [ 'RaaCounter', 'header' ] );
	add_action( 'wp_footer', [ 'RaaCounter', 'footer' ] );
}