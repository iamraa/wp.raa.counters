<div class="wrap">
	<h2>RAA: счетчики</h2>

	<form method="post" action="options.php">
		<?php wp_nonce_field( 'update-options' ); ?>
		<?php settings_fields( 'raa-counter' ); ?>

		<table class="form-table">

			<tr valign="top">
				<th scope="row">Google Analytics ID:</th>
				<td><input type="text" name="raa-counter[ga]" value="<?= RaaCounter::$options['ga']; ?>"/><br>UA-0000000-0</td>
			</tr>

            <tr valign="top">
                <th scope="row">Accurate Bounces:</th>
                <td><input type="checkbox" name="raa-counter[accurate.bounce]" value="1" <?= !empty(RaaCounter::$options['accurate.bounce']) ? 'checked' : ''; ?>/></td>
            </tr>

			<tr valign="top">
				<th scope="row">Yandex.Metrika ID:</th>
				<td><input type="text" name="raa-counter[yam]" value="<?= RaaCounter::$options['yam']; ?>"/><br>0000000</td>
			</tr>

            <tr valign="top">
                <th scope="row">Yandex.Metrika Webvisor:</th>
                <td><input type="checkbox" name="raa-counter[yam.webvisor]" value="1" <?= !empty(RaaCounter::$options['yam.webvisor']) ? 'checked' : ''; ?>/></td>
            </tr>

		</table>

		<input type="hidden" name="action" value="update"/>

		<p class="submit">
			<input type="submit" class="button-primary" value="<?php _e( 'Save Changes' ) ?>"/>
		</p>

	</form>
</div>
